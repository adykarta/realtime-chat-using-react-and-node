// use express
var express = require("express");

// create instance of express
var app = express();

// use http with instance of express
var http = require("http").createServer(app);

var port = 8080;
http.listen(port, function () {
	console.log("Listening to port " + port);
});

var io = require("socket.io")(http);
io.on("connection", function (socket) {
	// this is socket for each user
    console.log("User connected", socket.id);
    console.log("connection established")
    socket.on("newMessage", function (data) {
        io.emit("newMessage", data)
        console.log("Client says", data);
    });
    socket.on('disconnect', function(data){
        console.log("disconnected")
    })
});
