import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const MessageBox = ({onSendMessage:pushSendMessage})=>{
    const classes = useStyles();

    const [message, setMessage] = useState('');
 

    return (
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="standard-basic" label="Standard" value={message} 
        onChange={(e)=>setMessage(e.target.value)}
        onKeyDown = {e=>{
            if(e.key==="Enter"){
                e.preventDefault();
                pushSendMessage(message)
                setMessage('')
            }
        }}
        />
     
      </form>
    );
}
export default MessageBox;