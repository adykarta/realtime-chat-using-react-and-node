import React,{Fragment} from 'react';
import MessageBox from './MessageBox';
import UseChat from './UseChat'
import Message from './Message';

const Chat = ()=>{
  const {messages, sendMessage} =  UseChat();

    return (
        <Fragment>
        <Message
        messages={messages}
        />
      <MessageBox
      onSendMessage={message=>{
          sendMessage({message})
      }}
      />
      </Fragment>
    );
}
export default Chat;