import {useEffect, useState, useRef} from 'react';
import socketIOClient from 'socket.io-client';

const UseChat = ()=>{
    const socketRef = useRef()
    const [messages, setMessage] = useState([])
    console.log(messages)
    useEffect(()=>{
        socketRef.current = socketIOClient('http://localhost:8080/')

        socketRef.current.on("newMessage", ({message})=>{
            console.log(message)
            setMessage(prev=>[...prev, message])
        })
        return()=>{
            socketRef.current.disconenct();
        }
    },[])
    const sendMessage = ({message})=>{
        socketRef.current.emit('newMessage', {message})
    }

    console.log(messages)
    return{messages, sendMessage}
}
export default UseChat;