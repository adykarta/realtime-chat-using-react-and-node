import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Message({messages}) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([1]);
 
  return (
    <List dense className={classes.root}>
    {messages.map((message,index)=>{
        return(
            <ListItem key={index} button>
            <ListItemAvatar>
              <Avatar
                alt={`Avatar n°${message + 1}`}
                src={`/static/images/avatar/${message + 1}.jpg`}
              />
            </ListItemAvatar>
            <ListItemText id={index} primary={message} />
        
          </ListItem>
        )
    })}
    </List>
  );
}